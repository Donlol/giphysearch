//
//  GiphyFetcher.h
//  Giphy Search
//
//  Created by don lol on 04/08/15.
//  Copyright (c) 2015 Nomtek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GiphyFetcher : NSObject

- (void)gifsForName:(NSString *)name withCompletionBlock:(void (^)(NSMutableArray* gifs, NSError *error))completion;

@end
