//
//  main.m
//  Giphy Search
//
//  Created by don lol on 04/08/15.
//  Copyright (c) 2015 Nomtek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
