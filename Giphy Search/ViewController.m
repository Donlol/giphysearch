//
//  ViewController.m
//  Giphy Search
//
//  Created by don lol on 04/08/15.
//  Copyright (c) 2015 Nomtek. All rights reserved.
//

#import "ViewController.h"
#import "GiphyFetcher.h"
@interface ViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@property (strong, nonatomic) UISearchBar *searchBar;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) GiphyFetcher *giphyFetcher;
@property (strong, nonatomic) NSArray *gifs;
@end

@implementation ViewController

NSString * const reuseIdentifier = @"";

- (void)viewDidLoad
{
    [super viewDidLoad];
    CGFloat height = 50.f;
    self.searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, height)];
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, height, self.view.frame.size.width, self.view.frame.size.height-height)];
    
    self.edgesForExtendedLayout=UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars=NO;
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.title = @"Giphy Search";
    [self.view addSubview:self.searchBar];
    [self.view addSubview:self.tableView];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.searchBar.delegate = self;
}

#pragma mark - UITableViewDataSource and UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    cell.textLabel.text = [NSString stringWithFormat:@"Mam indeks: %ld",(long)indexPath.row];
    
    return cell;
}

#pragma mark - UISearchBarDelegate
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSLog(@"Wyszukalem: %@", searchBar.text);
    
    [searchBar resignFirstResponder];
    
    [self.giphyFetcher gifsForName:searchBar.text withCompletionBlock:^(NSArray *gifs, NSError *error) {
    
    }];
}

#pragma mark - accesors


- (GiphyFetcher *)giphyFetcher
{
    if(!_giphyFetcher)
    {
        _giphyFetcher = [[GiphyFetcher alloc]init];
    }
    return _giphyFetcher;
}






@end
